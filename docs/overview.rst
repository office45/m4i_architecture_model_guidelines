========
Overview
========

.. _overview:

.. toctree::
    :maxdepth: 2
    :numbered:

Collaboration on Enterprise Architecture (EA) models requires that all architects represent comparable situations in the same way. This is necessary to
 - better be able to read the models either by architects or business users.
 - make the EA model fit together, i.e. different parts of the model can be related with each other.
 - ...
 
Each organization defines its own Architecture Modeling Guidelines. Below is a set of recommended guidelines, which ensures that the different parts of your architecture nicely fit together and can be related in a meaningful way.  

Having agreed upon Architecture Modeling Guidelines enables to check whether the actual model follows these guidelines. These checks can identify concepts and relationships which are not compliant with a particular Architecture Modeling Guideline using specific non-compliance conditions. The number of non-compliant elements over the number of checked elements per check is called a consistency metric. 
Thus, a consistency metric provides an indication if there are non-compliant concepts and further provides a list of the identified non-compliant concepts.

A consistency metric might be applicable to multiple parts of the Architecture Modeling Guidelines, further, to check a guideline multiple consistency metrics might be required. Therefore the Architecture Modeling Guidelines and the consistency metrics form a matrix. The relationships are documented in each guideline and consistency metric respectively. Further, there is an overview of the mapping at <ADD LINK>.

Organization may want to deviate for certain concepts from a particular Architecture Modeling Guideline for specific reasons, this is called an exemption. In this case an organization can mark for a specific check a specific concept to be exempted, i.e. it will not be recorded as a non compliance, but as an exemption and therefore will no longer be listed as an exemption. < LOOKUP OTHER TEXT FROM USER GUIDELINES>

In the following Architecture Modeling Guidelines are introduced before the consistency metrics and the related checks are explained. 
