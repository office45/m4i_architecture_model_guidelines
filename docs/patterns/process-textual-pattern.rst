.. include:: substitutions.rst

|TextualPattern|
=================


Motivation
----------

To increase the homogenity of the enterprise architecture model and to make it easier to read and understand for non-expert users, it is beneficial to have conventions on how the different concepts are named. In this architecture modeling guideline conventions are described.

Description
-----------

The following conventions for labeling enterprise architecture model concepts as defined:

* Concepts and views are labeled in sentence format, i.e., starting with a single upper case character and continue with lower case characters.
* A concept label can not contain the text "(copy)".
* Behavioral elements are labeled with verbs, while active and passive structures are labeled with nouns.

Example
-------

|img-textual-pattern-example|


Related guidelines
------------------

None

Related consistency metrics
---------------------------

None

.. |img-textual-pattern-example| image:: media/guidelines/process-textual-example.png
   :width: 3.13in