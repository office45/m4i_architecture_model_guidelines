.. include:: substitutions.rst

|LocationSeparationPattern|
===========================


Motivation
----------


Description
-----------


Example
-------

|img-location-separation-example|


Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-location-separation-example| image:: media/guidelines/physical-location-separation-example.png
   :width: 6.26806in
