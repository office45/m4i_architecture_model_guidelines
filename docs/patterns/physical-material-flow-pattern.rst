.. include:: substitutions.rst

|MaterialFlowPattern|
=====================


Motivation
----------


Description
-----------


Example
-------

|img-material-flow-example|


Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-material-flow-example| image:: media/guidelines/physical-material-flow-example.png
   :width: 6.26806in
