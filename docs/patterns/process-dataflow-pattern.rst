.. include:: substitutions.rst

|DataflowPattern|
=================


Motivation
----------

The dataflow pattern serves as an essential building block for processes. It specifies how the information flow on business and application layer is represented. Based on the overall principle to explicate as much as possible information in ArchiMate concepts, the information flow, i.e. the exchange of data and information between different behavioral elements, is represented by using passive structures, thus **business objects** and **data objects**, between behavioral elements using **access** relations. Be aware that there are four flavors of the access relation: 

* **access read**: accessing data without altering them
* **access write**: writing data without reading them before
* **access read-write**: reading data and afterwards writing the changed data
* **access**: where it is not specified how the data are accessed.

|img-dataflow-pattern|


Description
-----------

The dataflow on business and application level is represented by explicating **business objects** and **data objects** and relating them with behavioral elements using one of the four **access** relations. Relevant behavioral elements for this pattern are:

* **business processes**, and **business functions** on the business layer, and 
* **application processes** and **application functions** on the application layer.

The aim of this pattern is to explicate the information flow by representing which information is exchanged between a business process and the subsequent business process. Thus, similar to the |SequencePattern|, the usage of **flow** relations to represent information exchanged is not supported in this guideline, since it only implies information exchange but does not explicate the exchanged information.


Example
-------

|img-dataflow-pattern-example|

The example depicts the control and dataflow of a process, where a project member reports hours, which then get approved by the project manager and later on get assigned to the project total spent hours.
The control flow is represented by the **trigger** relations (according to the |SequencePatternRef|). The dataflow is represented by various **business objects**, which are related to the **business processes** via **access read** and **access write** relations.

Related guidelines
------------------

- |SequencePatternRef|


Related consistency metrics
---------------------------



.. |img-dataflow-pattern| image:: media/guidelines/process-dataflow.png
   :width: 3.13in
.. |img-dataflow-pattern-example| image:: media/guidelines/process-dataflow-example.png
   :width: 3.13in
