.. include:: substitutions.rst

|SubProcessPattern|
=================


Motivation
----------

Processes are often described on different levels of abstractions, i.e. the high level process, which is then described in more detail on an intermediate abstraction level, where the process steps can be further worked out as another process on a detailed abstraction level. To ensure that the process representations on the different abstraction levels are consistent, it is important that the nested processes are not crossing the boundaries of the nested process to relate to other activities.
Thus, all activities inside a subprocess are only having **trigger** relations with activities of the same subprocess. Further, to express the nesting relationship of a subprocess, all activities in a subprocess are related to the parent process using **aggregation** or **composition** relations.


Description
-----------

The pattern applies to business, application or technology level.
All processes and functions in a subprocess are related to the parent process on the same ArchiMate layer using **aggregation** or **composition** relations. Due to the nesting each process or function can have at most one parent process, thus can be the destination of at most one **aggregation** or **composition** relation.
Further, activities in a subprocess can only be connected via **trigger** relations with other activities in the same subprocess. This way the boundaries of the subprocess are preserved, which guarantees decomposition of processes into subprocesses. 

This applies to the following activities in the subprocess:

- **business processes**, and **business functions** on the business layer, 
- **application processes** and **application functions** on the application layer, and
- **technology processes** and **technology functions** on the technology layer.

Further, subprocesses should have a single **start** and **end event** in the subprocess to clearly mark the start and the end of the process.


Example
-------

|img-subprocess-pattern-example|

 The left hand side shows an *effort reporting* business function and an *authentication and authorization* business function. The subprocesses of the two parent processes are related via **trigger** relations. This is not allowed according to the guideline, since a subprocess is not in itself closed. There are no start and end events and trigger relations are crossing subprocess boundaries.
 
 On the right hand side the same scenario is modelled in a different way. Now the *login process* is a business process in the *effort reporting* business function and the *authentication and authorization* business function is serving the *login process*. This way the boundaries of the subprocess are preserved. It is expressed that the *login process* has a dependecy to another function, which on an application level might be represented by an application services implementing an interface.
 

Related guidelines
------------------

* |ProcessSecondaryHierarchyRef|
* |RelationshipHierarchyRef|

Related consistency metrics
---------------------------


.. |img-subprocess-pattern-example| image:: media/guidelines/process-subprocess-example.png
   :width: 6.26806in
