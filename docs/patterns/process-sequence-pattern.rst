.. include:: substitutions.rst

|SequencePattern|
=================


Motivation
----------

The Sequence pattern serves as the fundamental building block for processes. It is used to construct a series of consecutive activities, which are executed in turn one after the other. The **trigger** relationship expresses that the first activity is completed before the subsequent activity is started. 

|img-sequence-pattern|


Description
-----------

An activity on business, application or technology level is enabled after the completion of a preceding activity. In particular, this pattern applies to

- **business events**, **business processes**, and **business functions** on the business layer, 
- **application events**, **application processes** and **application functions** on the application layer, and
- **technology events**, **technology processes** and **technology functions** on the technology layer.

The ArchiMate concepts may be directly connected via **trigger** relations or may use **junctions** to explicate the meaning of multiple incoming or outgoing **trigger** relations.

This does not prevent the usage of **trigger** relationships involving the other behavioral elements of each layer, but enforces the usage of **trigger** relations between the mentioned ArchiMate concept types.


Example
-------

|img-sequence-pattern-example|

The example shows that the process starts with an event indicating that a user wants to report hours. Then it is checked whether the user is already logged in. If this is not the case the user has to login. Afterwards the user can proceed to report hours.


Related guidelines
------------------

- |RelationshipAnnotationPatternRef|
- |RelationshipSemantcisRef|


Related consistency metrics
---------------------------

- |SingleTriggerFlowRef|
- |EventsProcessesRelsMetric|
- |ProcessesJunctionsRelsMetricRef|


.. |img-sequence-pattern| image:: media/guidelines/sequence-pattern.png
   :width: 6.26806in
.. |img-sequence-pattern-example| image:: media/guidelines/process-sequence-pattern-example.png
   :width: 3.13in