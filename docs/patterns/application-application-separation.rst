.. include:: substitutions.rst

|ApplicationSeparationPattern|
==============================


Motivation
----------


Description
-----------


Example
-------

|img-application-separation-pattern|


Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-application-separation-pattern| image:: media/guidelines/application-application-separation.png
   :width: 6.26806in
