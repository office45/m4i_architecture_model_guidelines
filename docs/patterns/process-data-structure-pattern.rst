.. include:: substitutions.rst

|DataStructurePattern|
======================


Motivation
----------

Data structures are essential when describing processes. The aim is to represent data on different abstraction layers within a specific ArchiMate layer. The aim is e.g. to say that a purchase order consists out of multiple order lines, where each order line contains the product number, the number of units being ordered, as well as the resulting amount. Hierarchical structures are modeled using **composition** and **aggregation** relations. 

|img-data-structure-pattern|

The challenge is on how to represent columns having the same name, but are representing different concepts. In the second half of the above example the data structure of an account is represented. In both cases there is a data structure **Amount**. While they have different meaning due to the different context, they also have something in common - both concepts represent an financial amount with a currency. In this pattern the approach is to keep the concepts - belong to different contexts - separate and extend the concept name by a context annotation. To represent that the two concepts are related with each other, a semantic model is added where the two concepts are a **specialization** of a concept **Amount** representing the common meaning.   


Description
-----------

A data structure is represented between **business objects** on the business layer and **data objects** on the application layer using **composition** and **aggregation** relations.

A semantic model represents that two concepts have different names, although have the same meaning, where the meaning is represented as a concept on its own. The concepts are related via **specialization** relations. The names of all concepts have to be unique.


Example
-------

|img-data-structure-pattern-example|





Related guidelines
------------------

* |DataflowPatternRef|


Related consistency metrics
---------------------------



.. |img-data-structure-pattern| image:: media/guidelines/process-data-structure-pattern.png
   :width: 6.26806in
.. |img-data-structure-pattern-example| image:: media/guidelines/process-data-structure-pattern-example.png
   :width: 6.26806in
