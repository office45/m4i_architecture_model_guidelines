.. include:: substitutions.rst

|AsynchronouseInterfacePattern|
===============================


Motivation
----------


Description
-----------

|img-asynchronouse-interface-pattern|

* Interfaces are part of a system
* Systems interact via interfaces
* Each interfaces is related to a service making this interface accessible to the business layer
* Interfaces of two applications are interacting with each other represented as a serving relationship
* Services are independent of each other in case of asynchronous interaction
* Business process also requires a trigger to consume the exchanged data
* Derived trigger relationship between interfaces
* Services are serving a business process
* Derived serving relationship between application and business process
* Data object allows to specify which data is exchanged by calling the interface


Example
-------

|img-interface-pattern-example|


Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-asynchronouse-interface-pattern| image:: media/guidelines/application-asynchronous-patterm.png
   :width: 6.26806in

.. |img-interface-pattern-example| image:: media/guidelines/application-interface-example.png
   :width: 6.26806in