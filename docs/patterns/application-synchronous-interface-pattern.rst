.. include:: substitutions.rst

|SynchronouseInterfacePattern|
===============================


Motivation
----------


Description
-----------

|img-synchronouse-interface-pattern|

* Interfaces are part of a system
* Systems interact via interfaces
* Each interfaces is related to a service making this interface accessible to the business layer
* Interfaces of two applications are interacting with each other represented as a serving relationship
* Business processes are connected via a trigger in case of synchronous interaction
* Services are calling each other in case of synchronous interaction (derived relationship)
* Derived trigger relationship between interfaces
* Services are serving a business process
* Derived serving relationship between application and business process
* Data object allows to specify which data is exchanged by calling the interface


Example
-------

|img-interface-pattern-example|

Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-synchronouse-interface-pattern| image:: media/guidelines/application-synchronous-patterm.png
   :width: 6.26806in
   
.. |img-interface-pattern-example| image:: media/guidelines/application-interface-example.png
   :width: 6.26806in
   