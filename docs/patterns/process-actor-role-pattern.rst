.. include:: substitutions.rst

|ActorRolePattern|
==================


Motivation
----------

This pattern is another fundamental building block for processes. It is used to explicate who is responsible for performing a particular **business function** or **business process** and how these responsibilities are distributed to different business units or people via different organizational roles.


Description
-----------

A **business actor** or **business role** is related to a **business process** or **business function** via an **assignment** relationship indicating that the active structure is responsible for executing the activity.
In some case it can be beneficial to use **business roles** to represent that a **business actor** e.g. a business unit has different roles inside the organization. In this case again **assignment** relationships are used from **business actors** to **business roles**.

**Business collaboration** is another active internal ArchiMate structure element, which would be used to explicate that multiple roles are work together on a behavior. However, in this guideline this element is not included to keep the processes and the active structures directly connected. It contradicts the principle of explicating as much knowledge in ArchiMate concepts as possible, however, lesser number of concept types makes it easier for non experts to consume the models. Further, this guideline assumes that most process will have only a single active structure being assigned and in case there are multiple that it is implicitly a collaboration.


Example
-------

|img-actor-role-pattern|

The generic explanation of the example is that the actors Bob and Charlie have a project member role which requires them to report their hours for the project. The reported hours then have to be approved by the project manager, i.e. Charlie. Alice has the role of a controller, who again checks the hours and bills the hours to the project.

In this example roles are used to abstract from actors and describe responsibilities in terms of a project. This allows to represent that Charlie is fulfilling multiple roles. On the left hand side, the process of reporting hours and the related process is project specific. If another project with another project manager would share the same process, then project manager of one project may approve reported hours of another project. 

To be able to re-use the process in multiple projects, this guideline suggest to separate the process organization and the process specific roles and relate them via **specialization** relations. A possible representation is depicted on the right hand side. The project specific roles for the test project are aggregated and the individual roles test project roles are related via **specialization** relationships to the project related roles.


Related guidelines
------------------



Related consistency metrics
---------------------------


.. |img-actor-role-pattern| image:: media/guidelines/process-actor-role-pattern.png
   :width: 6.26806in
