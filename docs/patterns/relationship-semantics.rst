.. include:: substitutions.rst


|RelationshipSemantcis|
=======================

.. author Andreas

Motivation
----------

In several situations a junction can clarify whether incoming or outgoing relationships must all be fulfilled or it is sufficient to only fulfill a single relationship. The two meanings are expressed as **and junctions** and **or junctions**.

Junctions connect relationships of the same relationship type. Further, they are applicable to all dynamic and dependency relationships, as well as **assignment** and **realization** relationships. In particular, **serving**, **access**, **influence**, **association**, **trigger**, **flow**, **assignment**, and **realization** relationships.

In the context of this guideline the relevant relationships are **trigger** and **flow** relationships.


Description
-----------

Based on this guideline ArchiMate concepts can only have a single incoming or outgoing **trigger** or **flow** relationship. In case of the need to have multiple incoming or outgoing **trigger** or **flow** relationships then these relationships have to be connected to a junction, which then has to be connected to corresponding ArchiMate concept.


Example
-------

|img-relationship-semantics|

In this example the left hand side shows 



Related guidelines
------------------

- |SequencePatternRef|



Related consistency metrics
---------------------------

- |SingleTriggerFlowRef|


.. |img-relationship-semantics| image:: media/guidelines/relationship-semantics.png
   :width: 6.26806in