.. include:: substitutions.rst


|ProcessSecondaryHierarchy|
===========================

.. author Andreas

Motivation
----------

<MODIFY currently is a copy and paste>

Enterprise Architects use hierarchies to represent different levels of abstraction in an Enterprise Architecture model. To ensure that these abstraction levels are maintainable and consistent it is beneficial to constrain how hierarchies can be formed. Besides the pure relationship based constrained addressed in this guideline there are additional constraints which are also dependent on the connected ArchiMate concept types. However, all hierarchies have in common that they are acyclic and tree-structured.


Description
-----------
This guideline in particular addresses structural hierarchies build using **aggregation** and **composition** relations, and specialization hierarchies constructed from **specialization** relations. 
A specialization hierarchy is a set of enterprise architecture model concepts, 

* where each concept is connected with a **specialization** relation to another concept in the hierarchy, and
* where the concept is not connected via **specialization** relationship to any other enterprise architecture model concept not included in the hierarchy.

A structural hierarchy is defined in the same way, but then based on **composition** and **aggregation** relationships.
A hierarchy must have the following properties:  

* Hierarchies are non cyclic, i.e. there is no path of relationships in the hierarchy, where the path is not empty and the start and end concept of the path are the same concept.
* A single hierarchy can be described as a tree, i.e. every concept in the hierarchy has exactly one parent and has none, a single, or multiple child concepts. 


Example
-------

|img-relationship-hierarchy|

On the left hand side is a Enterprise Architecture model, which describes on a low abstraction level the reporting of hours that requires a login of the user. This functionality is described on an higher abstraction level as *Effort reporting*. The three business processes and the two business functions form a hierarchy, which is non-cyclic, but is not forming a tree. The business process *Login process* has two parent nodes, i.e. business function *Effort reporting* and business function *Authentication and authorization*. Thus, it is not clear on which nodes form the primary hierarchy used for creating the different abstraction layers (see also :ref:`hierarchical processes<hierarchy-process>` ).

The right hand side complies with the architecture modeling guideline, since the red **aggregation** relationship on the left hand side has been replaced by a **serving** relationship (also marked red). Thus, the three business processes and the business function *Effort reporting* now form a hierarchy, which is non cyclic and form a tree. This way, it is trqansparent that the *Effort reporting* has been used as a primary hierarchy and a structure around *Authentication and authorization* is used as secondary hierarchy. 


Related guidelines
------------------

* |RelationshipHierarchyRef|

 

Related consistency metrics
---------------------------

None

.. |img-relationship-hierarchy| image:: media/guidelines/relationship-hierarchy.png
   :width: 6.26806in