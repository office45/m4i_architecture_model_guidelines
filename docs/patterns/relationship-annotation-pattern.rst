.. include:: substitutions.rst

|RelationshipAnnotationPattern|
===============================

.. author Andreas

Motivation
----------

Enterprise Architects sometimes make use of annotations to explicate a certain constraint or an associated content on the relationship. What exactly the meaning of the annotation is depends on the interpretation of the reader of the model. In this guidelines the principle is to explicate as much as possible the meaning in ArchiMate constructs to reduce the ambiguity and to enable analysis of the created Enterprise Architecture models. 



Description
-----------

Relationships may have textual annotations. This guideline encourages the Enterprise Architect to explicate the meaning of a possible annotation in terms of ArchiMate concepts. 
Therefore the recommendation is to avoid annotations of relationships as much as possible. Acceptable annotations are **Yes** and **No**. 


Example
-------

|img-relationship-annotation|

On the left hand side an example is provided with relationship annotations, while the right hand side shows the same process following this relationship annotation guideline.

On the left hand side constrains on when a particular **trigger** relationship could be executed are annotated. On the right hand side this check is represented as a explicit **business process** which enables then the use of simple **Yes** and **No** relationship annotations. Based on the *Is logged in?* business process the *Login process* business process can be executed when the question is answered by **No**, otherwise the actual task can directly be executed.

Additional examples are described :ref:`earlier<relationship-annotations>`.



Related guidelines
------------------

None.


Related consistency metrics
---------------------------

- |SentenceRef|


.. |img-relationship-annotation| image:: media/guidelines/relationship-annotation.png
   :width: 6.26806in