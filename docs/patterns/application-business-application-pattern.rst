.. include:: substitutions.rst

|BusinessApplicationPattern|
============================


Motivation
----------


Description
-----------
|img-business-application-pattern|


Example
-------



Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-business-application-pattern| image:: media/guidelines/application-business-application.png
   :width: 6.26806in
