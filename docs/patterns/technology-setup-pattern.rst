.. include:: substitutions.rst

|TechnologySetupPattern|
========================


Motivation
----------


Description
-----------


Example
-------

|img-technology-setup-pattern|


Related guidelines
------------------



Related consistency metrics
---------------------------
.. |img-technology-setup-pattern| image:: media/guidelines/technology-setup.png
   :width: 6.26806in
