===================
Physical guidelines 
===================

.. include:: substitutions.rst

.. _physical:

	
Introduction
=============  



In the following the related patterns are described.


.. _MaterialFlowPattern:
.. include:: patterns/physical-material-flow-pattern.rst

.. _LocationPattern:
.. include:: patterns/physical-location-pattern.rst

