===================
Consistency metrics
===================

.. include:: substitutions.rst

.. _consistency-metrics:

Introduction
============

Architecture Model Guidelines are describing the rules Enterprise Architects being involved in the development of an Enterprise Architecture shared Architecture model in a Models4Insight project. To ensure that all modelers comply with the guidelines, it is beneficial to check the guidelines. The **compliance** of the EA model with the Architecture Model Guidelines is measured by **the number of compliant concepts over the number of tested concepts** - this is called a **consistency metrics**. To ensure that a guideline is followed, it might be necessary to test multiple consistency metrics. Guidelines do not translate one on one to consistency metrics, because a single consistency metric might be applicable to multiple guidelines and the other way around.

In the following the different consistency metrics are introduced and their semantics are explained. Further a mapping to the related Architecture Model Guidelines is made.

.. _SingleTriggerFlow:
.. include:: metrics/single-trigger-flow.rst


.. _UnconnectedConcepts:
.. include:: metrics/unconnected-concepts.rst

.. _ProcessTriggerFlow:
.. include:: metrics/process-trigger-flow.rst


.. _TriggerContext:
.. include:: metrics/trigger-context.rst


.. _Sentence:
.. include:: metrics/sentence.rst


.. _CopyText:
.. include:: metrics/copy-text.rst


.. _ActorsRolesRelsMetric:
.. include:: metrics/actors-roles-rels-metric.rst


.. _ProcessesJunctionsRelsMetric:
.. include:: metrics/process-junctions-rels-metric.rst


.. _EventsProcessesRelsMetric:
.. include:: metrics/events-processes-rels-metric.rst


.. _BetweenFacilitiesRelsMetric:
.. include:: metrics/between-facilities-rels-metric.rst


.. _EquipmentFacilitiesRelsMetric:
.. include:: metrics/equipment-facility-rels-metric.rst

.. _TreePropertyMetric:
.. include:: metrics/tree-property-metric.rst
