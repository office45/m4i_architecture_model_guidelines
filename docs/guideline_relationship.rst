=======================
Relationship guidelines
=======================

.. include:: substitutions.rst

.. _relationships:


Introduction
=============

The guidelines described in this section are introduced on the business layer, but are applicable to all layers and also cross layer.

.. _relationship_annotations:

Relationship annotations
------------------------

ArchiMate allows to add text to relationships and there are quite some examples making quite extensive use of this capability. The challenge with these annotations is that it is left to the interpretation of the reader, whether 

* the annotation represents a business object associated to the relation, 
* it is a condition which must be fulfilled before the subsequent business process can be executed, 
* it is a specific business role which can execute the subsequence business process, or
* it is a specific resource which is required to execute the business process.
 
The recommendation in this guideline is to explicate the meaning and therefore avoid relationship labels as much as possible. In particular, it is recommended to limit relationship labels to the labels **Yes** and **No**. 

.. _relationships-with-relationships:

Relationships with relationships
--------------------------------

Since ArchiMate 3 it is possible to define relationships, where the source or target of the relationship is another relationship. This is sometimes used e.g. by relating **business processes** via **flow** relationships and **associate** a **business object** with the **flow** relationship. The associated business object explicates which information is exchanged between the business processes represented by the **flow** relationship.

This guideline suggests to avoid relationships to relationships as much as possible and provides alternatives, by e.g. representing the exchange of a business object between two business processes using the **access write** and **access read** relationships. Thus, in the provided guidelines there will not be any relationship to relationship examples.

.. _hierarchical-relationships:

Hierarchical relationships
--------------------------

Hierarchical relationships are not an ArchiMate category of relationships, but describe that parent concepts depend on specific child concepts. Hierarchical relationships are used to 

* organize or abstract from detailed information - when looked at it from bottom up, or
* describe a decomposition or drill down - when looked at it from a top down perspective.

To represent this semantic in ArchiMate, the most appropriate relationships are structural relationships:

* **Composition** relationship, if the child concept must be present in the parent concept, or
* **Aggregation** relationship, if the child concepts may be present in the parent concept. 

Further, hierarchies between concepts of the same type can be defined using **specialization** relationships. This special relationship has the property that only transitivity can be derived using **specialization** relationships, while **aggregation** and **composition** relationships provide more derived relationships. 

In general hierarchies have the following properties:  

* A structural hierarchy is build of parent and child concept types using **composition** and **aggregation** relationships, while a specialization hierarchy consists only of **specialization** relationships. Both are hierarchies. (`ArchiMate connectivity table <https://pubs.opengroup.org/architecture/archimate3-doc/apdxb.html#_Toc10045493>`__ ).
* Hierarchies are non cyclic, i.e. there is no path of relationships in the hierarchy, where the path is not empty and the start and end concept of the path are the same concept. There are no cycles for the combination of **composition** and **aggregation** relations as well as there is no cycle of **specialization** relations.
* A single hierarchy can be described as a tree, i.e. every concept in the hierarchy has exactly one parent and has none, a single, or multiple child concepts. This applies to structural as well as specialization hierarchies.

In case more than one hierarchy for a set of concepts is required, than a child concept could end up with multiple parent concepts. In this situation, the approach is to choose one primary hierarchy and define the remaining hierarchies as secondary hierarchies. While **aggregation** and **composition** relationships are used for the primnary hierarchy, other dependency relationships like the **serving** relationship can be used for the secondary hierarchies (see also :ref:`hierarchical processes<hierarchy-process>` ).


.. _RelationshipAnnotationPattern: 
.. include:: patterns/relationship-annotation-pattern.rst


.. _RelationshipSemantcis: 
.. include:: patterns/relationship-semantics.rst

.. _RelationshipHierarchy: 
.. include:: patterns/relationship-hierarchy.rst

