================================
Architecture Modeling Guidelines
================================

.. include:: substitutions.rst

.. _guidelines:

Introduction
============



.. _RelationshipGuidelines:
.. include:: guideline_relationship.rst


.. _ProcessGuidelines:
.. include:: guideline_process.rst
