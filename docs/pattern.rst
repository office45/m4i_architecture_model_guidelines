=====================================
Pattern Architecture Model Guidelines
=====================================
.. _pattern:

.. toctree::
    :maxdepth: 2
    :numbered:

Introduction
============
There are different design patterns defined by different people and organizations. There are many resources
describing patterns on different levels, like e.g. processes, data, infrastructure, or collaborations.

To validate design patterns it is necessary to check whether these patterns are actually followed. These 
checks can be automated and expressed in numbers of concepts, which are compliant, non-compliant or are 
considered to be an exception of the design pattern.

Thus, a design pattern can be checked using one or multiple consistency metrics, while a consistency metrics
give some indications on the compliance with one or multiple design patterns.

In the following, different design patterns and consistency metrics are described with a short description,
an example, and a motivation. 


Process patterns
================
test test

Sequence pattern
----------------

Description
~~~~~~~~~~~
A process step on business, application or technology level is enabled after the completion of a preceding process step.

Example
~~~~~~~
A receipt is printed after the train ticket is issued.

Motivation
~~~~~~~~~~
The Sequence pattern serves as the fundamental building block for processes. It is used to construct a series of consecutive tasks, which execute in turn one after the other. Two tasks form part of a Sequence if there is a trigger relation from one of them to the next which has either no label at all, or the label True or False.

<add a picture here>


