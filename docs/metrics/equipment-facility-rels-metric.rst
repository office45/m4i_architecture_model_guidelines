.. include:: substitutions.rst

|EquipmentFacilitiesRelsMetric|
===============================

.. author Dijon

Description
-----------


Non-compliance conditions
-------------------------

Equipment and Facilities can be related via assignment relationships


Non-compliance report
---------------------

The non-compliance report contains non-compliant relationships described by the following columns:

- Source ID, name and type:	UUID, name and ArchiMate type of the source concept of the reported relationship
- Relationship ID and type: UUID and relationship type causing the non-compliance
- Target ID, name and type:	UUID, name and ArchiMate type of the target concept of the reported relationship


Resolution
----------

Replace the non-compliant relationship by an **assignment** relationship.


Related consistency metrics
---------------------------

Related guidelines
------------------


