.. include:: substitutions.rst

|SingleTriggerFlow|
===================

.. author Andreas

Description
-----------

ArchMate allows that a single concepts has multiple incoming or outgoing **trigger** or **flow** relationships. This leaves inclear what the meaning of the multiple relations is: do all of them have to be fulfilled or only a single one? 
Thus, this guideline requires that there is only a single **trigger** or **flow** relationship ingoing or outgoing of a concept. The semantics of **tigger** and **flow** relationships can be expressed by using junctions. This applies to multiple concepts on business, application, and technology layer. 


Non-compliance conditions
-------------------------

The following conditions are checked:

- Each **business process**, **business function**, **business event**, and **business interface** has more than one incoming or outgoing **trigger** or **flow** relationship.
- Each **application process**, **application function**, **application event**, and **application interface** has more than one incoming or outgoing **trigger** or **flow** relationship.
- Each **technology process**, **technology function**, **technology event**, and **technology interface** has more than one incoming or outgoing **trigger** or **flow** relationship.


Non-compliance report
---------------------

The non-compliance report contains non-compliant concepts and an indication on what is causing the non-compliance described by the following columns:

- ID, name and type: ID, name and ArchiMate type of the concept, where multiple incoming or outgoing **trigger** or **flow** relations have been observed
- Relationship type: Which relationship type is causing the non-compliance
- Relationship direction: Are the non-compliane relationships ingoing or outgoing?
- # of relationships: Number of relationships causing the non-compliance.


Resolution
----------

The non-compliance at a concept is caused by multiple ingoing or outgoing relationships. The non-compliance can be resolved by adding an **and-** or **or-junction** between the concepts and the target or source of the non-compliant relationships. Further, add a **trigger** or **flow** relationship between junction and reported concept.

|single-trigger-relation-resolution-pic|

In the example above the left hand side shows the non-compliant scenario and a possible resolution on the right hand side. **Business process 2** on the left hand side will be reported as non-compliant with two incoming **trigger** relationships. On the right hand side the semantics of the two incomig **trigger** relations is explicated by adding an **or-junction**. 

Please make sure that the newly created junction is added to all views where the effected relationships are used. Otherwise these views will no longer contain the corresponding **trigger** or **flow** relationship.


Related consistency metrics
---------------------------


Related guidelines
------------------

- |RelationshipSemantcisRef|



.. |single-trigger-relation-resolution-pic| image:: media/metrics/single-trigger-relation-resolution.png
   :width: 6.26806in