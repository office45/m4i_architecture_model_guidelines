.. include:: substitutions.rst

|Hierarchical Relations|
=======================

.. author Dijon

Description
-----------

When constructing relationships between elements using composition, aggregation or specializaion relationships you aim to create parts in a model that represent a hierarchy of concepts.
Aggregation, Composition and Specializaiton Relationship between elements with allowed relationships as described in the Archimate Specifications Appendix `Relationship Tables<https://pubs.opengroup.org/architecture/archimate3-doc/apdxb.html#_Toc10045493>`_.


Non-compliance conditions
-------------------------

An element violates the tree property when it has two parent elements connected via an aggregation or composition relation.

- Each child should have a single parent 
- We filter out elements not connected via aggregation, composition or specialization relations


Non-compliance report
---------------------

The non-compliance report contains non-compliant elements described by the following columns:

====================    ===========================================================
Child ID                The identifier of the common child element.
Element Name            The name of the common child element.
Element Type            The ArchiMate type of the common child element.
# of Parents            Total number of Parents of the common child element.
Parent Element Type     The ArchiMate type(s) of Parents with the common child element.'
Relation type           The ArchiMate type of the relation between the child and parents.
====================    ===========================================================


Resolution
----------

The resolution can be to remove one of the relationship between the child and of it's parents.

It could also be an intentional and in that case we suggest looking at the semantic model. To resolve the non-compliance using the semantic model pattern you would replace the two relationships between the violating child element and the parent elements with two seperate elements that each have a specialization relaionship to the original child element. As a result the two new elements would represent a specialized version of the original child element and maintain the hierachy of concepts. 



Related consistency metrics
---------------------------




Related guidelines
------------------

