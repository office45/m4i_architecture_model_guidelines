.. include:: substitutions.rst

|Cycle Detection|
=======================

.. author Dijon

Description
-----------

A model can be represented as a graph. In graph theory a cycle occurs when there is a closed path between two nodes. That means by starting at an arbitrary element in the model, you follow a specific path between elements and relations, that reaches back at the element you started from. The set of elements that are present in such a closed path form a cycle.

The part of an Enterprise Architecture model represented by aggregation, composition and specializatoin. One property to ensure a hierarchical ordering is the exclusion of cycles.


Non-compliance conditions
-------------------------

A set of elements are non-compliant if they are connected via a mix of aggregation an and composition and form a cycle. 

Another set of element are non-compliant if they are connected via specialization relations and form a cycle.


Non-compliance report
---------------------

The non-compliance report contains two sections. The first is Detected Cycles which includes elements connected via a mix of composition and aggregation relationships. The second is Detected Specialization Cycles that only contains elements connected via specialization relations. 

Both sections are described by the following columns:

==============   ===========================================================================
Elements ID     The identifiers of the elements in the cycle
Cycle Size      The number of elements in the cycle
Element Types   The ArchiMate types of the elements in the cycle
Relation Types  The ArchiMate type of the connecting relationships between elements in cycle
==============  ============================================================================


Resolution
----------

Examine the set of elements forming a cycle to see which elements and relationships combinations must be adjusted to better reflect the nature of the modelled processes. 


Related consistency metrics
---------------------------

- Hierarchical Relations


Related guidelines
------------------

