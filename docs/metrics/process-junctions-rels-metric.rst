.. include:: substitutions.rst

|ProcessesJunctionsRelsMetric|
==============================

.. author

Description
-----------


Non-compliance conditions
-------------------------

Check the following:

- Business processes can be related via trigger, composition or aggregation relationship
- Application processes can be related via trigger, composition or aggregation relationship
- Technology processes can be related via trigger, composition or aggregation relationship
- A Business, application or technology process can be related to an And/Or junction via a trigger relationship
- A Business, application or technology process can be related to an And/Or junction via a flow relationship (not part of this metric, instead part of FacilityJunctionsRelsMetric, but implemented in this metric)

Non-compliance report
---------------------

The non-compliance report contains non-compliant relationships described by the following columns:

- Source ID, name and type:	UUID, name and ArchiMate type of the source concept of the reported relationship
- Relationship ID and type: UUID and relationship type causing the non-compliance
- Target ID, name and type:	UUID, name and ArchiMate type of the target concept of the reported relationship


Resolution
----------

Replace the non-compliant relationship by an **assignment** relationship.


Related consistency metrics
---------------------------

Related guidelines
------------------

