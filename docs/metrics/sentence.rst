.. include:: substitutions.rst

|Sentence|
==========

.. author Andreas

Description
-----------
 regular expression: r"^[A-Z1-90][a-zA-Z1-90\.\-_\(\)\[\])]*([ ][A-Za-z1-90\(\)\[\]][a-zA-Z1-90\.\-_\(\)\[\])]*|$)*$")

Non-compliance conditions
-------------------------

The following conditions are checked:

- All concept and view labels must be in sentence format.
- Labels of relationships must be empty, **Yes** or **No**.

Non-compliance report
---------------------

The non-compliance report contains non-compliant relationships described by the following columns:

- Source ID, name and type:	UUID, name and ArchiMate type of the source concept of the reported relationship
- Relationship ID and type: UUID and relationship type causing the non-compliance
- Target ID, name and type:	UUID, name and ArchiMate type of the target concept of the reported relationship


Resolution
----------

Replace the non-compliant relationship by an **assignment** relationship.


Related consistency metrics
---------------------------

Related guidelines
------------------
