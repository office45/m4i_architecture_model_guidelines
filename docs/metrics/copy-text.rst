.. include:: substitutions.rst

|CopyText|
==========

.. author Dijon

Description
-----------

When constructing an Enterprise Architecture model your aim is to maintian a clear distinction between concepts and relationships. It can occur that you create or copy a concept and give it the same label and Archimate type of an already existing concept. The labels used should be unique to differentiate between their purpose and function. There shouldn't be duplication of concept name and type or duplication of the same relationship between two concepts.

Additionally, when using Archi it can easily happen that during a copy operation of a concept from one view to another view a concept gets duplicated. If Archi duplicates a concept it appends the text “(Copy)” to the original concept name. 

This metric finds these label, concept and relationship duplications and reports them to the user.

Using Archi it can easily happen that during a copy operation of a concept from one view to another view a concept gets duplicated. If Archi duplicates a concept it appends the text "(Copy)" to the original concept name. This metric finds these concepts and reports them t the user.

Non-compliance conditions
-------------------------

A concept is non-compliant if it contains the text "(Copy)" in it's label name.

A set of concept are non-complant if two or more concepts with unique identifiers have the same name and Archimate type.

A set of relationships are non-compliant if they are of the same Archimate type and are connecting the same source and target concept.


Non-compliance report
---------------------

The non-compliance report contains sections for the three types of duplications: Copy Text, Concept, and Element duplication.

For the resepctive sections it contains non-compliant columns described by the following columns:

=============   ===========================================================
Copy Text 
---------------------------------------------------------------------------
=============   ===========================================================
ID              The identifier of the concept
Concept Name    The name of the concept
Type            ArchiMate type as either an Element, View or Relationship
=============   ===========================================================

=============   ===========================================================
Elements 
---------------------------------------------------------------------------
=============   ===========================================================
ID              The identifier of the concept
Name            The name of the concept
Element Type    The ArchiMate type of the element
Frequency       The number of duplicates
=============   ===========================================================

====================   =======================================================================
Relationships 
----------------------------------------------------------------------------------------------
====================   =======================================================================
Relationship ID        The identifier of the relationship
Relationship type      The type of the relationship
Elements Type          The type of the source and target elements
Source element name    The name of the source element
Target element name    The name of the target element
# of Duplicates        The number of duplicate relationships between source and target element
====================   =======================================================================


Resolution
----------

In case of "(Copy)" the label should be update to exclude the text while ensuring the name is unique.

A concept can be reported as non-compliant, because

- it has been accidentially duplicated,
- it has been intentiannly duplictaced, but the label of the concept has not been adjusted, or
- the (Copy) text has been intentionally used by the enterprise architect in the model.

In the first case, all relations with the non-compliant element should be changed to the intended concept, while in the second case, the label has to be adjusted. In the last case, the non-compliance can be marked as an exemption.

In case of duplicate concepts the judgement should be made if a different label should be given or if it is indeed the same concept and should be reused in that view. It could also be the case that it represents the same concept but is a different instance. In that case a naming convention could be set in place to differentiate between them for example by appending a number or code. 

In case of a relationship duplication between two concepts the suggestion is to simply remove the redundant relationship. It could be that it was added by accident because the one present wasn't in the view.


Related consistency metrics
---------------------------

None


Related guidelines
------------------

None
