.. include:: substitutions.rst

|UnconnectedConcepts|
=====================

.. author Andreas

Description
-----------

The aim of an Enterprise Architecture model is to show the relations between all involved concepts. Thus, the expectation is that all concepts are one way or the other related with each other. If there are concepts which are not connected to any other element then they will require special attention. The same applies if there are *partitions* in the Enterprise Architecture model, i.e., a collection of concepts and relationships which are connected with each other, but are unrelated to other concepts or relationships.

Isolated concepts are partitions consisting only of a single concept.

Non-compliance conditions
-------------------------

A concept is non-compliane if it has no incoming and outgoing relationship.

A model is non-compliant if it has more than one partition, where a partition is a set of concepts and relationships connected with each other, but where there is no concept in a partition A that is connected to an element of another partition B.


Non-compliance report
---------------------

The non-compliance report contains two parts: The list of unconnected concept and the report on the observed partitions and their characteristics.

Unconnected elements are described by their ID, name and ArchiMate type.

If there are multiple fragments, the non-compliance report contains per fragment:
 
- the number of included concepts 
- the number of included relationships


Resolution
----------

In case of unconnected concepts it could be because the concept has been added by accident and therefore can be removed, or because there are still relationships missing connecting it to the remaining model, thus, the missing relationships have to be added.

In case of partitioning it is much more difficult. The first step is to understand what the different partitions are and what they are representing. Based on this understanding possibilities of relating the different partitions can be defined.

Please note that partitions are derived on the basis of the underlying Enterprise Architecture model and are not based or related to the views contained in the model.


Related consistency metrics
---------------------------



Related guidelines
------------------


