.. include:: substitutions.rst

|TreePropertyMetric|
====================

.. author Dijon

Description
-----------



Non-compliance conditions
-------------------------

Aggregation and Composition Relationship between:

- same concept types 
- business function and business process
- application function and application process
- technology function and technology process


An element violates the tree property when it has two parent elements connected via an aggregation or composition relation.

- Each  source / child should have a single parent / target
- We filter out elements not connected via aggregation or composition


Non-compliance report
---------------------

The non-compliance report contains non-compliant relationships described by the following columns:

- Source ID, name and type:	UUID, name and ArchiMate type of the source concept of the reported relationship
- Relationship ID and type: UUID and relationship type causing the non-compliance
- Target ID, name and type:	UUID, name and ArchiMate type of the target concept of the reported relationship


Resolution
----------

Replace the non-compliant relationship by an **assignment** relationship.



Related consistency metrics
---------------------------

Related guidelines
------------------

