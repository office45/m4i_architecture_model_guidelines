.. include:: substitutions.rst

|EventsProcessesRelsMetric|
===========================

.. author Qu

Description
-----------

Events and process on the different business, application and technology ArchiMate layer are connected via **trigger** relationships to explicate potential data flow using **business objects**, **data objects** or **artifacts** depending on the ArchiMate layer. Events on the different business, application and technology ArchiMate layer can also be connected to **And/Or junctions** using **trigger** relationships. 

The relationship between processes on the business, application and technology ArchiMate layer to **And/Or junctions** are described in the |ProcessesJunctionsRelsMetricRef| metric.


Non-compliance conditions
-------------------------

A relationship is non-compliant if :

- it connects a **business events** and **business processes**, which is not a **trigger** relationship.
- it connects a **application events** and **application processes**, which is not a **trigger** relationship.
- it connects a **technology events** and **technology processes**, which is not a**trigger** relationship.
- it connected **business**, **application** or **technology event** and an **And/Or junction**, which is not a **trigger** relationship.

<Qu's text:>
If **events** and **processes** are related via **trigger**, there can be **and-junctions/or-junctions** in-between to create a **path** of sequential relationships 

- A relationship is non-compliant if it is part of a **path** and related via a different relationship than **trigger** relationship




Non-compliance report
---------------------

The non-compliance report contains non-compliant relationships described by the following columns:

==================== =========================================================================================
Event triggers process
--------------------------------------------------------------------------------------------------------------
Column               Description
==================== =========================================================================================
Relationship ID      relationship UUID (Universally Unique Identifier) causing the non-compliance

Relationship type    relationship type causing the non-compliance

Source name          name of the source concept of the reported relationship

Source type          ArchiMate type of the source concept of the reported relationship

Target name          name of the target concept of the reported relationship

Target type          ArchiMate type of the target concept of the reported relationship
==================== =========================================================================================

If the relationships are part of a **path** connected to junctions, non-compliant relationships are described by the following columns:

==================== =============================================================================================================
Event triggers process with junctions
----------------------------------------------------------------------------------------------------------------------------------
Column               Description
==================== =============================================================================================================
Relationship ID      relationship UUID (Universally Unique Identifier) causing the non-compliance

Relationship type    relationship type causing the non-compliance

Start name           name of the event start/process start of the path the reported relationship is a part of

Start type           ArchiMate type of the event start/process start of the path the reported relationship is a part of

Source type          ArchiMate type of the source concept of the reported relationship

Target type          ArchiMate type of the target concept of the reported relationship

End name             name of the event end/process end of the path the reported relationship is a part of

End type             ArchiMate type of the event end/process end of the path the reported relationship is a part of
==================== =============================================================================================================


Resolution
----------

A non-compliant relationship can be corrected by replacing the non-compliant relationship with a **trigger** relationship.

Please make sure that the newly created relationship is added to all views where the non-compliant relationships was used. Otherwise these views will no longer contain a relationship between the concepts, which were originally connected by the non-compliant relationship.



Related consistency metrics
---------------------------

- |ProcessesJunctionsRelsMetricRef|


Related guidelines
------------------

- |SequencePatternRef|
