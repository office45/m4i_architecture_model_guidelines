.. include:: substitutions.rst

|Elements Missing in Views|
=======================

.. author Dijon

Description
-----------

When creating models in Archi you often create, delete and reuse elements. There is a potential when deleting an element you only delete it from the View and never re-use it again. When this occurs an element becomes redundant because it is still present in the model but not being used in any Views.



Non-compliance conditions
-------------------------

An element is non-compliant if it is present in the model but not present in any View.


Non-compliance report
---------------------

The non-compliance report contains non-compliant elements described by the following columns:

=============   ===========================================================
ID              The identifier of the element
Element Name    The name of the elemet
Type            ArchiMate type of the Element
=============   ===========================================================


Resolution
----------

Delete the unused elements from the model.

Related consistency metrics
---------------------------

None

Related guidelines
------------------

None
