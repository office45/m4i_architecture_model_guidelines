.. Guidelines

.. |BusinessApplicationPattern| replace:: Business application pattern
.. |BusinessApplicationPatternRef| replace:: :ref:`Business application pattern<BusinessApplicationPattern>`

.. |SynchronouseInterfacePattern| replace:: Synchronouse interface pattern
.. |SynchronouseInterfacePatternRef| replace:: :ref:`Synchronouse interface pattern<SynchronouseInterfacePattern>`

.. |AsynchronouseInterfacePattern| replace:: Asynchronouse interface pattern
.. |AsynchronouseInterfacePatternRef| replace:: :ref:`Asynchronouse interface pattern<AsynchronouseInterfacePattern>`

.. |TextualPattern| replace:: Textual pattern
.. |TextualPatternRef| replace:: :ref:`Textual pattern<TextualPattern>`

.. |SequencePattern| replace:: Sequence pattern
.. |SequencePatternRef| replace:: :ref:`Sequence pattern<SequencePattern>`

.. |ActorRolePattern| replace:: Actor role pattern
.. |ActorRolePatternRef| replace:: :ref:`Actor role pattern<ActorRolePattern>`

.. |DataflowPattern| replace:: Dataflow pattern
.. |DataflowPatternRef| replace:: :ref:`Dataflow pattern<DataflowPattern>`

.. |DataStructurePattern| replace:: Data structure pattern
.. |DataStructurePatternRef| replace:: :ref:`Data structure pattern<DataStructurePattern>`

.. |SubProcessPattern| replace:: Subprocess pattern
.. |SubProcessPatternRef| replace:: :ref:`Subprocess pattern<SubProcessPattern>`

.. |ProcessSecondaryHierarchy| replace:: Secondary hierarchy pattern
.. |ProcessSecondaryHierarchyRef| replace:: :ref:`Secondary hierarchy pattern<ProcessSecondaryHierarchy>`

.. |RelationshipAnnotationPattern| replace:: Relationship annotation pattern
.. |RelationshipAnnotationPatternRef| replace:: :ref:`Relationship annotation pattern<RelationshipAnnotationPattern>`

.. |RelationshipSemantcis| replace:: Explicate relationship semantics pattern
.. |RelationshipSemantcisRef| replace:: :ref:`Explicate relationship semantics pattern<RelationshipSemantcis>`

.. |RelationshipHierarchy| replace:: Relationship hierarchy pattern
.. |RelationshipHierarchyRef| replace:: :ref:`Relationship hierarchy pattern<RelationshipHierarchy>`

.. |MaterialFlowPattern| replace:: Material flow pattern
.. |MaterialFlowPatternRef| replace:: :ref:`Material flow pattern<MaterialFlowPattern>`

.. |LocationSeparationPattern| replace:: Location separation pattern
.. |LocationSeparationPatternRef| replace:: :ref:`Location separation pattern<LocationSeparationPattern>`

.. |TechnologySetupPattern| replace:: Technology setup pattern
.. |TechnologySetupPatternRef| replace:: :ref:`Technology setup pattern<TechnologySetupPattern>`


.. Metrics

.. |SingleTriggerFlow| replace:: Single trigger and flow relationship
.. |SingleTriggerFlowRef| replace:: :ref:`Single trigger and flow relationship<SingleTriggerFlow>`

.. |ActorsRolesRelsMetric| replace:: Actor role assignment
.. |ActorsRolesRelsMetricRef| replace:: :ref:`Actor role assignment<ActorsRolesRelsMetric>`

.. |ProcessesJunctionsRelsMetric| replace:: Process event flow
.. |ProcessesJunctionsRelsMetricRef| replace:: :ref:`Process event flow<ProcessesJunctionsRelsMetric>`

.. |CopyText| replace:: Copy text
.. |CopyTextRef| replace:: :ref:`Copy text<CopyText>`

.. |Sentence| replace:: Sentence
.. |SentenceRef| replace:: :ref:`Sentence<Sentence>`

.. |EventsProcessesRelsMetric| replace:: Events processes metric
.. |EventsProcessesRelsMetricRef| replace:: :ref:`Events processes metric<EventsProcessesRelsMetric>`

.. |BetweenFacilitiesRelsMetric| replace:: Facility structure
.. |BetweenFacilitiesRelsMetricRef| replace:: :ref:`Facility structure<BetweenFacilitiesRelsMetric>`

.. |EquipmentFacilitiesRelsMetric| replace:: Equipment facility assignment
.. |EquipmentFacilitiesRelsMetricRef| replace:: :ref:`Equipment facility assignment<EquipmentFacilitiesRelsMetric>`

.. |ProcessTriggerFlow| replace:: Process trigger flow
.. |ProcessTriggerFlowRef| replace:: :ref:`Process trigger flow<ProcessTriggerFlow>`

.. |TriggerContext| replace:: Trigger context
.. |TriggerContextRef| replace:: :ref:`Trigger context<TriggerContext>`

.. |UnconnectedConcepts| replace:: Unconnected concepts
.. |UnconnectedConcepts| replace:: :ref:`Unconnected concepts<UnconnectedConcepts>`

.. |TriggerContext| replace:: Trigger context
.. |TriggerContextRef| replace:: :ref:`Trigger context<TriggerContext>`

.. |UnconnectedConcepts| replace:: Unconnected concepts
.. |UnconnectedConceptsRef| replace:: :ref:`Unconnected concepts<UnconnectedConcepts>`

.. |TreePropertyMetric| replace:: Tree property
.. |TreePropertyMetricRef| replace:: :ref:`Tree property<TreePropertyMetric>`

.. |DistributionNetworks| replace:: Distribution networks
.. |DistributionNetworksRef| replace:: :ref:`Distribution networks<DistributionNetworks>`

