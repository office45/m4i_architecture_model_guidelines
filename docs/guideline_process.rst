==================
Process guidelines 
==================

.. include:: substitutions.rst

.. _processes:

	
Introduction
=============  

Processes are defined on business, application and technology level. Processes on technology level are also used for the physical level. The patterns described in this section are introduced on the business layer, but are applicable to all layers.

Processes describe a sequence of activities using business processes and business functions. Sequences of activities in ArchiMate are best expressed using **trigger** relationships. Often organizations also use **flow** relationships between **business processes** and **business functions** to indicate that there is a data dependency between the various concepts. The recommendation in this guideline is to explicate data dependencies by representing them as **business objects**, and use **access** relationships to express that a **business process** reads, write, or read/write the **business object**. The **business object** could also be assiciated with the **flow** relationship, however, in this guideline the aim is to avoid relationships with relationships (more :ref:`here<relationships-with-relationships>`).

An activity often has an active concept assigned, which performs the activity. In ArchiMate this is expressed by an **assignment** relationship, between a **business role** or a **business actor** and a business process. The recommendation is to explicate the active concept as much as possible instead of keeping it implicit in the description of the business process. 

The ArchiMate standard describes a **business function** as a collection of activities to structure activities based on a set of criteria, while a business process is an activity, which achieves a certain result. Along these lines in this guideline **business processes** represent activities performed, while **business functions** are used to group **business processes** or **business functions** according to some criteria. It is therefore possible to have multiple sets of criteria to organize the **business processes** into different **business functions**. 

.. _hierarchy-process: 

To represent these hierarchical relationships this guideline recommends to use **composition** or **aggregation** relationships between a **business function** or a **business process** and a **business process**. Usually the model is organized with a specific mind set, which results in one set of criteria to be dominant over the other ones. What the criteria are and why these are dominant depends on the specific use case and can not be generalized. It is important that this set of criteria is comprehensible preferably in the part of the organization the model is created for.

In this guideline it is recommended to relate **business functions** or **business processes** to **business processes** organized by the remaining sets of criteria using a **serving** relationship. This approach has multiple benefits:

* the main design direction remains clearly visible,
* additional sets of criteria remain applicable representing different decompositions of a business process or business function 
* the different decompositions form a hierarchy (see :ref:`Hierarchical relationships<_hierarchical-relationships>` ).

In the following the related patterns are described.


.. _SequencePattern:
.. include:: patterns/process-sequence-pattern.rst

.. _DataFlowPattern:
.. include:: patterns/process-dataflow-pattern.rst

.. _DataStructurePattern:
.. include:: patterns/process-data-structure-pattern.rst

.. _ActorRolePattern:
.. include:: patterns/process-actor-role-pattern.rst

.. _SubProcessPattern:
.. include:: patterns/process-subprocess-pattern.rst

.. _SecondaryHierarchyPattern:
.. include:: patterns/process-secondary-hierarchy.rst

.. _TextualPattern:
.. include:: patterns/process-textual-pattern.rst
