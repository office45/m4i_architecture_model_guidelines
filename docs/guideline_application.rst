==================
Application guidelines 
==================

.. include:: substitutions.rst

.. _application:

	
Introduction
=============  


.. _BusinessApplicationPattern:
.. include:: patterns/application-business-application-pattern.rst

.. _SynchronousInterfacePattern:
.. include:: patterns/application-synchronous-interface-pattern.rst

.. _AsynchronousInterfacePattern:
.. include:: patterns/application-asynchronous-interface-pattern.rst

.. _ApplicationSeparationPattern:
.. include:: patterns/application-application-separation.rst

